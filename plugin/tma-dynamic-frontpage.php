<?php

/*
  Plugin Name: TMA Dynamic Frontpage
  Plugin URI: https://thorstenmarx.com/projekte/experience-platform/experience-manager/
  Description: The integration for the experience platform.
  Author: Thorsten Marx
  Version: 2.0.0
  Author URI: https://thorstenmarx.com/
  Text Domain: tma-webtools
  Domain Path: /languages
 */

require_once ('includes/tma-frontpage-options.php' );


add_action('tma_dynamic_frontpage', 'tma_dynamic_frontpage');
add_action('update_option_tma_dynamic_frontpage', 'tma_dynamic_frontpage_options');

function tma_dynamic_frontpage_check_and_set_frontpage($from, $to, $page) {
	$from_date = DateTime::createFromFormat('Y-m-d', $from);
	$to_date = DateTime::createFromFormat('Y-m-d', $to);

	//$temp = new DateTime("now");
	//$now = DateTime::createFromFormat('Y-m-d', $temp->format("Y-m-d") );
	$now = new DateTime("now");
	$now->setTime(0, 0, 0);
	$from_date->setTime(0, 0, 0);
	$to_date->setTime(0, 0, 0);

	if ($now >= $from_date && $to_date >= $now) {
		update_option('page_on_front', $page);
		return TRUE;
	} else {
		return FALSE;
	}
}

function tma_dynamic_frontpage_options($options) {
	tma_log("tma_dynamic_frontpage_options");
	$active = $options["active"];
	$default_page = $options["default"];

	var_dump($options);

	if ($active == 1) {
		$check_and_set_result = FALSE;
		if (isset($options["from_1"]) && isset($options["to_1"])) {
			tma_log("1");
			$from = $options["from_1"];
			$to = $options["to_1"];
			$page = $options["page_on_front_1"];

			$check_and_set_result = tma_dynamic_frontpage_check_and_set_frontpage($from, $to, $page);
		}

		if ($check_and_set_result === FALSE) {
			if (isset($options["from_2"]) && isset($options["to_2"])) {
				tma_log("2");
				$from = $options["from_2"];
				$to = $options["to_2"];
				$page = $options["page_on_front_2"];

				$check_and_set_result = tma_dynamic_frontpage_check_and_set_frontpage($from, $to, $page);
			}
		}

		if ($check_and_set_result === FALSE) {
			update_option('page_on_front', $default_page);
			update_option('show_on_front', 'page');
		}
	}
}

function tma_dynamic_frontpage() {
	tma_log("tma_dynamic_frontpage");
	$options = get_option('tma_df_dynamic_frontpage');

	tma_dynamic_frontpage_options($options);
}

function tma_log($message) {
//	$fd = fopen("tma.log", "a");
//	$str = "[" . date("Y/m/d h:i:s", time()) . "] " . $message;
//	fwrite($fd, $str . "\n");
//	fclose($fd);
}

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */


