jQuery(function ($) {
	$(document).ready(function () {
		$('#logo_image_url').click(function () {

			var logo_imag = wp.media({
				title: 'Select Logo Image',
				library: {type: 'image'},
				multiple: false,
				button: {text: 'Insert'}
			});

			logo_imag.on('select', function () {
				var logo_selection = logo_imag.state().get('selection').first().toJSON();
				for (prop in logo_selection) {
					console.log(prop);
				}
				$('#logo_image').val(logo_selection.url);
			});

			logo_imag.open();
		});

		var selectedTab = $(".nav-tab-active").attr("id");
		if (selectedTab === "tma-dyn-front") {
			$("#tma-storefront-options-form").submit(function (event) {
				// validate frontpage 1
				var valid = true;
				$(".tma-range").each(function () {
					var from = $(this).find(".tma-from").val();
					var to = $(this).find(".tma-to").val();
					

					if (from === "" && to === "") {
						valid = true;
					} else if ((from === "" && to !== "") || (to === "" && from !== "")) {
						$(this).find(".tma-error").html(tma_locale.range_validation_not_both_set);
						valid = false;
					} else if (from >= to) {
						$(this).find(".tma-error").html(tma_locale.range_validation_from_greater_then_to);
						valid = false;
					}
				});
				if (!valid) {
					event.preventDefault();
					return false;
				}
			});
		}
	});
});