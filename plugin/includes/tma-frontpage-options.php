<?php


class TMAFrontpageOptions {

	private $options_general;
	private $options_dynamic_frontend;

	public function __construct() {
		add_action('admin_menu', array($this, 'add_plugin_page'));
		add_action('admin_init', array($this, 'tma_df_options_init'));
	}

	public function add_plugin_page() {
		add_theme_page(
				__("Dynamic Frontpage", "tma-frontpage"), __("Dynamic Frontpage", "tma-frontpage"), 'manage_options', 'tma-dynamic-frontpage-settings', array($this, 'options_page')
		);
	}

	public function options_page() {
		$this->options_general = get_option('tma_df_general');
		$this->options_dynamic_frontend = get_option('tma_df_dynamic_frontpage');

		//$dynfront_Screen = ( isset($_GET['action']) && 'dynfront' == $_GET['action'] ) ? true : false;
		$dynfront_Screen = 'dynfront';
		?>
		<div class="wrap">
			<h1><?php echo __("Dynamic Frontpage settings", "tma-frontpage"); ?></h1>
			<h2 class="nav-tab-wrapper">
				<?php /* ?>
				<a href="<?php echo admin_url('admin.php?page=tma-dynamic-frontpage-settings'); ?>" class="nav-tab<?php if (!isset($_GET['action']) || isset($_GET['action']) && 'dynfront' != $_GET['action']) echo ' nav-tab-active'; ?>"><?php esc_html_e('General', "tma-storefront"); ?></a>
				<?php */ ?>
				<a id="tma-dyn-front" href="<?php echo esc_url(add_query_arg(array('action' => 'dynfront'), admin_url('admin.php?page=tma-dynamic-frontpage-settings'))); ?>" class="nav-tab<?php if ($dynfront_Screen) echo ' nav-tab-active'; ?>"><?php esc_html_e('Dynamic Frontpage', "tma-storefront"); ?></a> 
			</h2>

			<form id="tma-storefront-options-form" method="post" action="options.php"><?php
				if ($dynfront_Screen) {
					settings_fields('tma_df_dynamic_frontpage');
					do_settings_sections('tma-dynamic-frontpage-admin');

					submit_button();
				} else {
					settings_fields('tma_df_general');
					do_settings_sections('tma-storefront-general');

					submit_button();
				}
				?>
			</form>
		</div> <?php
	}

	public function register_general_settings () {
		register_setting(
				'tma_df_general', // Option group
				'tma_df_general', // Option name
				array($this, 'sanitize') // Sanitize
		);

		add_settings_section(
				'setting_section_id', // ID
				'All Settings', // Title
				array($this, 'print_section_info'), // Callback
				'tma-storefront-general' // Page
		);
		/*
		add_settings_field(
				'logo_image', 'Logo Image', array($this, 'logo_image_callback'), 'vaajo-setting-admin', 'setting_section_id'
		);*/
		add_settings_field(
				'error_page', 
				'Error Page', 
				function () {
					$this->page_callback("error_page", "tma_df_general[error_page]", $this->options_general['error_page']);
				}, 
				'tma-storefront-general', 
				'setting_section_id'
		);
	}
	public function tma_df_options_init() {
		
		$this->register_general_settings();

		register_setting(
				'tma_df_dynamic_frontpage', // Option group
				'tma_df_dynamic_frontpage', // Option name
				array($this, 'sanitize') // Sanitize
		);

		add_settings_section(
				'dynamic_frontpage_default_section_id', // ID
				'Default Frontpage', // Title
				function () {
			echo "<div>";
			echo __("Aktivate die dynamic frontpage feature and select a default page", "tma-frontpage");
			echo "</div>";
		}, 'tma-dynamic-frontpage-admin' // Page
		);
		add_settings_field(
				'Activate', 'Activate', 
				function () {
					$current = isset($this->options_dynamic_frontend['active']) ? $this->options_dynamic_frontend['active'] : false;
					$this->checkbox_callback("tma_df_dynamic_frontpage[active]", $current);
				}, 
				'tma-dynamic-frontpage-admin', 'dynamic_frontpage_default_section_id'
		);
		add_settings_field(
				'tma_default_page', __("Default page", "tma-frontpage"), function () {
			$this->page_callback("page_on_front_default", "tma_df_dynamic_frontpage[default]", $this->options_dynamic_frontend['default']);
		}, 'tma-dynamic-frontpage-admin', 'dynamic_frontpage_default_section_id'
		);

		add_settings_section(
				'dynamic_frontpage_section_id', // ID
				'FrontPage 1', // Title
				array($this, 'print_section_info'), // Callback
				'tma-dynamic-frontpage-admin' // Page
		);
		add_settings_field(
				'tma_visible1', __("Visible", "tma-frontpage"), function () {
			$this->range_callback("from_1", "to_1");
		}, 'tma-dynamic-frontpage-admin', 'dynamic_frontpage_section_id'
		);
		add_settings_field(
				'tma_frontpage1', __("Page", "tma-frontpage"), function () {
			$this->page_callback("page_on_front_1", "tma_df_dynamic_frontpage[page_on_front_1]", $this->options_dynamic_frontend['page_on_front_1']);
		}, 'tma-dynamic-frontpage-admin', 'dynamic_frontpage_section_id'
		);

		add_settings_section(
				'dynamic_frontpage_2_section_id', // ID
				'FrontPage 2', // Title
				array($this, 'print_section_info'), // Callback
				'tma-dynamic-frontpage-admin' // Page
		);


		add_settings_field(
				'tma_visible2', __("Visible", "tma-frontpage"), function () {
			$this->range_callback("from_2", "to_2");
		}, 'tma-dynamic-frontpage-admin', 'dynamic_frontpage_2_section_id'
		);
		add_settings_field(
				'tma_frontpage2', __("Page", "tma-frontpage"), function () {
			$this->page_callback("page_on_front_2", "tma_df_dynamic_frontpage[page_on_front_2]", $this->options_dynamic_frontend['page_on_front_2']);
		}, 'tma-dynamic-frontpage-admin', 'dynamic_frontpage_2_section_id'
		);
	}

	public function print_section_info() {
		//your code...
	}

	public function logo_image_callback() {
		printf(
				'<input type="text" name="tma_df_general[logo_image]" id="logo_image" value="%s"> <a href="#" id="logo_image_url" class="button" > Select </a>', isset($this->options_general['logo_image']) ? esc_attr($this->options_general['logo_image']) : ''
		);
	}

	private function range_description() {
		printf(
				'<div>%s</div>', __("Time period in which the page should be displayed", "tma-frontpage")
		);
	}

	public function range_callback($fromid, $toid) {
		// "from_1", "tma_df_dynamic_frontpage[from_1]", $this->options_dynamic_frontend['from_1']
		$fieldName = "tma_df_dynamic_frontpage[$fromid]";
		$value = $this->options_dynamic_frontend[$fromid];
		echo "<div class='tma-range'>";
		printf(
				'<input class="tma-from" type="date" name="%s" id="%s" value="%s">', $fieldName, $fromid, isset($value) ? esc_attr($value) : ''
		);
		$fieldName = "tma_df_dynamic_frontpage[$toid]";
		$value = $this->options_dynamic_frontend[$toid];
		printf(
				'&nbsp;-&nbsp;<input class="tma-to" type="date" name="%s" id="%s" value="%s">', $fieldName, $toid, isset($value) ? esc_attr($value) : ''
		);
		$this->range_description();
		echo "<div class='tma-error' style='color:red;' />";
		echo "</div>";
	}

	public function from_callback($id, $fieldName, $value) {
		printf(
				'<input type="date" name="%s" id="%s" value="%s">', $fieldName, $id, isset($value) ? esc_attr($value) : ''
		);
	}

	public function to_callback($id, $fieldName, $value) {
		printf(
				'<input type="date" name="%s" id="%s" value="%s">', $fieldName, $id, isset($value) ? esc_attr($value) : ''
		);
	}	

	public function checkbox_callback($fieldName, $value) {
		?>
		<input type="checkbox" name="<?php echo $fieldName; ?>" value="1" <?php checked(1, $value, true); ?> /> 
		<?php
	}

	public function page_callback($id, $fieldName, $selected) {
		echo "<label for='$fieldName'>";
		echo wp_dropdown_pages(array('id' => $id, 'name' => $fieldName, 'echo' => 0, 'show_option_none' => __('&mdash; Select &mdash;'), 'option_none_value' => '0', 'selected' => $selected));
		echo "</label>";
	}

	public function sanitize($input) {
		$new_input = array();
		if (isset($input['fb_url'])) {
			$new_input['fb_url'] = sanitize_text_field($input['fb_url']);
		}

		if (isset($input['hide_more_themes'])) {
			$new_input['hide_more_themes'] = sanitize_text_field($input['hide_more_themes']);
		}

		if (isset($input['logo_image'])) {
			$new_input['logo_image'] = sanitize_text_field($input['logo_image']);
		}
		if (isset($input['error_page'])) {
			$new_input['error_page'] = sanitize_text_field($input['error_page']);
		}
		
		if (isset($input['default'])) {
			$new_input['default'] = sanitize_text_field($input['default']);
		}
		if (isset($input['active'])) {
			$new_input['active'] = sanitize_text_field($input['active']);

			if (wp_get_schedule("tma_dynamic_frontpage") === FALSE) {
				wp_schedule_event(time(), 'hourly', 'tma_dynamic_frontpage');
			}
		} else {
			wp_clear_scheduled_hook("tma_dynamic_frontpage");
		}
		if (isset($input['from_1'])) {
			$new_input['from_1'] = sanitize_text_field($input['from_1']);
		}
		if (isset($input['to_1'])) {
			$new_input['to_1'] = sanitize_text_field($input['to_1']);
		}
		if (isset($input['page_on_front_1'])) {
			$new_input['page_on_front_1'] = sanitize_text_field($input['page_on_front_1']);
		}
		if (isset($input['from_2'])) {
			$new_input['from_2'] = sanitize_text_field($input['from_2']);
		}
		if (isset($input['to_2'])) {
			$new_input['to_2'] = sanitize_text_field($input['to_2']);
		}
		if (isset($input['page_on_front_2'])) {
			$new_input['page_on_front_2'] = sanitize_text_field($input['page_on_front_2']);
		}

		do_action("update_option_tma_dynamic_frontpage", $new_input);

		return $new_input;
	}

}

if (is_admin()) {
	$settings_page = new TMAFrontpageOptions();
}

function load_wp_media_files() {
	wp_enqueue_media();
	
	// Register the script
	wp_register_script('tma-storefront-js', get_stylesheet_directory_uri() . '/assets/admin_js.js', array('jquery'), '1.0', true);

// Localize the script with new data
	$translation_array = array(
		'range_validation_not_both_set' => __('From and to date must be set!', 'tma-frontpage'),
		'range_validation_from_greater_then_to' => __('From must be smaller then to!', 'tma-frontpage')
	);
	wp_localize_script('tma-frontpage-js', 'tma_locale', $translation_array);

	wp_enqueue_script('tma-frontpage-js', get_stylesheet_directory_uri() . '/assets/admin_js.js', array('jquery'), '1.0', true);
	wp_enqueue_script('tma-frontpage-js');
}

add_action('admin_enqueue_scripts', 'load_wp_media_files');
?>